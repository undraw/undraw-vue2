import Editor from './editor.vue'

Editor.install = (Vue: any) => {
  Vue.component('UEditor', Editor)
}

export default Editor
export { Editor }
