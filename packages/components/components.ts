import Fold from './fold'
import Divider from './divider'
import Editor from './editor'

export default [Fold, Divider, Editor]
