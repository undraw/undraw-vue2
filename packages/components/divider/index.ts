import Divider from './divider.vue'

Divider.install = (Vue: any) => {
  Vue.component('UDivider', Divider)
}
export default Divider
export { Divider }
