import Fold from './fold.vue'

Fold.install = (Vue: any) => {
  Vue.component('UFold', Fold)
}

export default Fold
export { Fold }
