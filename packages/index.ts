import components from './components/components'
export * from './components'

export default {
  install: (app: any) => {
    components.forEach((item: any) => {
      app.use(item)
    })
  }
}
