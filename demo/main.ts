import Vue from 'vue'
import App from './App.vue'

// import UndrawVue2 from 'undraw-vue2/dist'
// import 'undraw-vue2/dist/style.css'

import UndrawVue2 from '~/index'

Vue.use(UndrawVue2)

new Vue({
  render: h => h(App)
}).$mount('#app')
